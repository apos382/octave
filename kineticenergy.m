f=@(mass,velocity) (1/2).*mass.*(velocity.^2)*(10^-3);
temp=1;
printf("This script just calculates the Kinetic Energy of BBs. Please input the chronograph data below:\n")
mass=input("Type the BB mass in grams: ");
choice=menu("Select velocity unit",{"fps","mps"});
velocity=input("Type the BB velocity: ");

if (choice == 1) 
  velocity=velocity*0.3048;
  temp=0.3048;
endif
ke=f(mass,velocity);
printf("THe KE of the BB is %f Joules\n", ke)
v=@(ke,m) sqrt( (2*ke)./(m*10^-3));

m=[0:0.01:0.6];
plot((v(ke,m)/temp) ,m)

m=[0.2:0.01:0.5];
m=[0.2:0.01:0.5;round(v(ke,m)/temp)];

printf("Mass(grams) | Velocity\n")
m'
printf("Don't forget the inertia! More mass, means more resistance to a change in motion.")
clear all


#This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 